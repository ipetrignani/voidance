#!/bin/sh
#
# Part 2: Chroot.
# https://docs.voidlinux.org/installation/guides/chroot.html

# hostname
printf "hostname: "
read -r hostname
echo "$hostname" | tee /etc/hostname

# hardwareclock, timezone, keymap, font (ter-v16n)
nvi /etc/rc.conf

# locales
nvi /etc/default/libc-locales
xbps-reconfigure -f glibc-locales

# user

printf "username: "
read -r username
useradd --create-home "$username" --groups floppy,lp,audio,video,cdrom,optical,mail,storage,scanner,kvm,input,users,xbuilder

printf "password: "
read -r password
(
   echo "$password"
   echo "$password"
) | passwd "$username"

echo "permit persist $username" | tee /etc/doas.conf

# swap

dd if=/dev/zero of=/swapfile bs=1M count=1024 status=progress
chmod 600 /swapfile
mkswap /swapfile

echo "vm.swappiness = 10" | tee -a /etc/sysctl.conf

# mounts

cp /proc/mounts /etc/fstab

echo "/swapfile swap swap rw,noatime,discard 0 0
tmpfs /tmp tmpfs defaults,nosuid,nodev 0 0" | tee -a /etc/fstab

blkid -s PARTUUID | tee -a /etc/fstab

# /     rw,noatime,commit=600 0 1
# /boot                       0 2

nvi /etc/fstab

# bootloader

mkdir -p /boot/EFI/BOOT/
cp /usr/lib/syslinux/efi64/ldlinux.e64 /boot/EFI/BOOT/
cp /usr/lib/syslinux/efi64/syslinux.efi /boot/EFI/BOOT/BOOTX64.EFI

mkdir -p /boot/syslinux/

partuuid=$(blkid -s PARTUUID /dev/sda2 -o value)
# ver=$(find /boot -maxdepth 1 -type f | grep -Eo '[0-9].*[^.img]' -m1)
ver=$(find /boot/vmlinuz* | grep -Eo '[0-9].*' -m1)

echo "DEFAULT void
LABEL void
        LINUX /vmlinuz-$ver
        INITRD /initramfs-$ver.img
        APPEND root=PARTUUID=$partuuid" | tee /boot/syslinux/syslinux.cfg

# packages configuration

xbps-reconfigure -fa

echo "ignorepkg=linux-firmware-amd
ignorepkg=linux-firmware-broadcom
ignorepkg=linux-firmware-nvidia
ignorepkg=sudo
ignorepkg=wifi-firmware" | tee /etc/xbps.d/xbps.conf

xbps-remove -R linux-firmware-amd \
   linux-firmware-broadcom \
   linux-firmware-nvidia \
   sudo \
   wifi-firmware

# wlan:
# wpa_passphrase <ssid> [passphrase] >> /etc/wpa_supplicant/wpa_supplicant-wlp1s0.conf
# doas ln -s /etc/sv/wpa_supplicant /var/service/

ln -s /etc/sv/dhcpcd /etc/runit/runsvdir/default/
ln -s /etc/sv/openntpd /etc/runit/runsvdir/default/

project=devoid

cp -r "$project"/etc/X11/ /etc/
mv "$project" /home/"$username"/
