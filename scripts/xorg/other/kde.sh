#!/bin/sh
#
# KDE.
# SDDM = root Xorg.

# feed the good stuff

doas xbps-install -Sy \
  void-repo-multilib \
  void-repo-multilib-nonfree \
  void-repo-nonfree

doas xbps-install -Sy \
  alsa-utils \
  apulse \
  ark \
  base-devel \
  curl \
  dolphin \
  ffmpeg \
  firefox \
  git \
  gwenview \
  intel-media-driver \
  iucode-tool \
  kde5 \
  konsole \
  mesa-dri \
  mksh \
  mpv \
  noto-fonts-emoji \
  okular \
  p7zip \
  p7zip-unrar \
  vim \
  wget \
  xorg-minimal \
  xtools

doas xbps-remove -oOR

doas makewhatis -a

chsh -s /bin/mksh

# config files

project=voidance

cp "$HOME"/"$project"/.gitconfig "$HOME"
cp "$HOME"/"$project"/.mkshrc "$HOME"
cp "$HOME"/"$project"/.vimrc "$HOME"

doas cp -r "$HOME"/"$project"/etc/X11/xorg.conf.d/ /etc/X11/

# KDE theme

cp -r "$HOME"/"$project"/.local/ "$HOME"

wget https://github.com/ddnexus/equilux-theme/archive/equilux-v20181029.zip
unzip equilux-v20181029.zip
doas ./equilux-theme-equilux-v20181029/install.sh
rm -rf equilux*

wget -qO install.sh https://git.io/papirus-icon-theme-install
sed -i 's/sudo/doas/g' install.sh
sh install.sh

wget -qO install.sh https://git.io/papirus-folders-install
sed -i 's/sudo/doas/g' install.sh
sh install.sh

doas papirus-folders -C grey --theme Papirus-Dark
rm install.sh

# vim packages

mkdir -p "$HOME"/.vim/pack/
cd "$HOME"/.vim/pack/ || exit

git init

git submodule add https://github.com/nanotech/jellybeans.vim colors/start/jellybeans/
git submodule add https://github.com/tpope/vim-commentary plugins/start/commentary/
git submodule add https://github.com/tpope/vim-flagship plugins/start/flagship/
git submodule add https://github.com/tpope/vim-fugitive plugins/start/fugitive/
git submodule add https://github.com/tpope/vim-repeat plugins/start/repeat/
git submodule add https://github.com/tpope/vim-sleuth plugins/start/sleuth/
git submodule add https://github.com/tpope/vim-surround plugins/start/surround/
git submodule add https://github.com/tpope/vim-vinegar plugins/start/vinegar/
git submodule add https://github.com/dense-analysis/ale syntax/start/ale/

vim -c "helptags ALL" -cq

cd "$HOME" || exit

# services

doas rm /var/service/dhcpcd
doas rm /var/service/wpa_supplicant
doas rm /etc/wpa_supplicant/wpa_supplicant-*

doas ln -s /etc/sv/dbus /var/service/
doas ln -s /etc/sv/NetworkManager /var/service/
#doas ln -s /etc/sv/sddm /var/service/

# rootless Xorg (no SDDM)

doas sed -i 's/yes/no/g' /etc/X11/Xwrapper.config

# cleanup

doas rm -rf "$project"
doas reboot
