#!/bin/sh
#
# KDE Setup.

# feed the good stuff

doas xbps-install -Sy void-repo-multilib \
  void-repo-multilib-nonfree \
  void-repo-nonfree

doas xbps-install -Sy ark \
  base-devel \
  curl \
  dejavu-fonts-ttf \
  ffmpeg \
  firefox \
  git \
  gvim \
  gwenview \
  ImageMagick \
  intel-media-driver \
  iucode-tool \
  kde5 \
  kde5-baseapps \
  kvantum \
  mesa-dri \
  mksh \
  mpv \
  p7zip \
  p7zip-unrar \
  pulseaudio \
  wget \
  xorg-minimal \
  xtools \
  youtube-dl

doas xbps-remove -oOR

doas makewhatis -a

chsh -s /bin/mksh

# config files

project=voidance

cp "$HOME"/"$project"/.gitconfig "$HOME"
cp "$HOME"/"$project"/.mkshrc "$HOME"
cp "$HOME"/"$project"/.vimrc "$HOME"

doas cp "$HOME"/"$project"/etc/sddm.conf /etc/
doas cp -r "$HOME"/"$project"/etc/X11/xorg.conf.d /etc/X11/

# KDE theme

themes="$HOME"/.local/share/themes/
icons="$HOME"/.local/share/icons/

aurorae="$HOME"/.local/share/aurorae/themes/
color_schemes="$HOME"/.local/share/color-schemes/
kvantum="$HOME"/.config/Kvantum/
plasma="$HOME"/.local/share/plasma/
sddm=/usr/share/sddm/themes/

konsole="$HOME"/.local/share/konsole/

dracula=https://github.com/dracula/gtk/archive/master.tar.gz
dracula_icons=https://github.com/dracula/gtk/files/5214870/Dracula.zip
mcmojave=https://github.com/vinceliuice/McMojave-cursors/archive/master.tar.gz
colorscheme=https://raw.githubusercontent.com/dracula/konsole/master/Dracula.colorscheme

mkdir -p "$themes" \
  "$icons" \
  "$aurorae" \
  "$color_schemes" \
  "$kvantum" \
  "$plasma" \
  "$konsole" \
  "$wallpapers"

wget "$dracula"
tar xf master.tar.gz
mv gtk-master/ "$themes"/Dracula/

wget "$dracula_icons" -O master.zip
unzip master.zip
mv Dracula/ "$icons"

wget "$mcmojave" -O master.tar.gz
tar xf master.tar.gz
mv McMojave-cursors-master/dist/ "$icons"/McMojave/

cp -r "$themes"/Dracula/kde/aurorae/* "$aurorae"
cp -r "$themes"/Dracula/kde/color-schemes/* "$color_schemes"
cp -r "$themes"/Dracula/kde/kvantum/* "$kvantum"
cp -r "$themes"/Dracula/kde/plasma/* "$plasma"
doas cp -r "$themes"/Dracula/kde/sddm/* "$sddm"

wget "$colorscheme" -P "$konsole"

# vim packages

mkdir -p "$HOME"/.vim/pack/
cd "$HOME"/.vim/pack/ || exit

git init

git submodule add https://github.com/dracula/vim colors/start/dracula/
git submodule add https://github.com/tpope/vim-commentary plugins/start/commentary/
git submodule add https://github.com/tpope/vim-flagship plugins/start/flagship/
git submodule add https://github.com/tpope/vim-fugitive plugins/start/fugitive/
git submodule add https://github.com/tpope/vim-repeat plugins/start/repeat/
git submodule add https://github.com/tpope/vim-sleuth plugins/start/sleuth/
git submodule add https://github.com/tpope/vim-surround plugins/start/surround/
git submodule add https://github.com/tpope/vim-vinegar plugins/start/vinegar/
git submodule add https://github.com/dense-analysis/ale syntax/start/ale/

vim -c "helptags ALL" -cq

cd "$HOME" || exit

# services

doas ln -s /etc/sv/dbus /var/service/
doas ln -s /etc/sv/NetworkManager /var/service/
doas ln -s /etc/sv/sddm /var/service/

doas rm -rf "$project" *master*
doas reboot
