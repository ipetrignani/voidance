" ~/.vimrc

" Bootstrap {{{

if $VIM_BARE
  setglobal noloadplugins
  finish
endif

setglobal nocompatible
setglobal pastetoggle=<F2>

filetype plugin indent on

" }}}
" Moving around, searching, patterns, and tags {{{

" Default on
"
" setglobal startofline
setglobal cpoptions+=J
if has('vim_starting')
  setglobal noignorecase
endif
setglobal smartcase
setglobal incsearch
" Default "./tags,tags"
"
" setglobal tags=./tags;
setglobal include=
setglobal path=.,,

" autocmd FileType c,cpp           setlocal path+=/usr/include include&
" autocmd FileType sh,zsh,csh,tcsh setlocal include=^\\s*\\%(\\.\\\|source\\)\\s
" autocmd FileType dosbatch setlocal include=^call | let &l:sua = tr($PATHEXT, ';', ',')
" autocmd FileType sh,zsh,csh,tcsh,dosbatch let &l:path =
"       \ tr($PATH, has('win32') ? ';' : ':', ',') . ',.'
" autocmd FileType ruby setlocal tags-=./tags;

" }}}
" Displaying text {{{

if has('vim_starting') && exists('+breakindent')
  set breakindent showbreak=\ +
endif
setglobal display=lastline
setglobal scrolloff=1
setglobal sidescrolloff=5
setglobal lazyredraw
setglobal listchars=tab:>\ ,trail:-,extends:>,precedes:<

" }}}
" Windows {{{

setglobal laststatus=2
setglobal showtabline=2
" Unknown
"
" if empty(&g:statusline)
"   setglobal statusline=[%n]\ %<%.99f\ %y%h%w%m%r%=%-14.(%l,%c%V%)\ %P
" endif
" setglobal titlestring=%{v:progname}\ %{tolower(empty(v:servername)?'':'--servername\ '.v:servername.'\ ')}%{fnamemodify(getcwd(),':~')}%{exists('$SSH_TTY')?'\ <'.hostname().'>':''}
" setglobal iconstring=%{tolower(empty(v:servername)?v:progname\ :\ v:servername)}%{exists('$SSH_TTY')?'@'.hostname():''}
if has('vim_starting')
  setglobal nohidden
endif

nnoremap <C-J> <C-w>w
nnoremap <C-K> <C-w>W

" }}}
" GUI {{{

" setglobal printoptions=paper:letter
setglobal mousemodel=popup
" if $TERM =~# '^screen'
"   if exists('+ttymouse') && &ttymouse ==# ''
"     setglobal ttymouse=xterm
"   endif
" endif

" Default "a"
" if !has('gui_running') && empty($DISPLAY) || !has('gui')
"   setglobal mouse=
" else
"   setglobal mouse=nvi
" endif
" if exists('+macmeta')
"   setglobal macmeta
" endif
setglobal winaltkeys=no

function! s:font()
    return 'monospace 12'
endfunction

command! -bar -nargs=0 Bigger  :let &guifont = substitute(&guifont,'\d\+$','\=submatch(0)+1','')
command! -bar -nargs=0 Smaller :let &guifont = substitute(&guifont,'\d\+$','\=submatch(0)-1','')
nnoremap <M-->        :Smaller<CR>
nnoremap <M-=>        :Bigger<CR>

autocmd VimEnter *  if !has('gui_running') | set noicon background=dark | endif
autocmd GUIEnter * set background=dark icon guioptions-=T guioptions-=m guioptions-=e guioptions-=r guioptions-=L
autocmd GUIEnter * silent! colorscheme vividchalk
autocmd GUIEnter * let &g:guifont = substitute(&g:guifont, '^$', s:font(), '')
autocmd FocusLost * let s:confirm = &confirm | setglobal noconfirm | silent! wall | let &confirm = s:confirm

" }}}
" Messages and info {{{

setglobal confirm
setglobal showcmd
setglobal visualbell

" }}}
" Editing text and indent {{{

" Default "indent,eol,start"
" Backwards compatibility
" setglobal backspace=2
setglobal complete-=i     " Searching includes can be slow
setglobal formatoptions+=j
" silent! setglobal dictionary+=/usr/share/dict/words
setglobal infercase
setglobal showmatch
setglobal virtualedit=block

setglobal shiftround
setglobal smarttab
if has('vim_starting')
  set tabstop=8 softtabstop=0
  if exists('*shiftwidth')
    set shiftwidth=0 softtabstop=-1
  endif
  set autoindent
  set omnifunc=syntaxcomplete#Complete
  set completefunc=syntaxcomplete#Complete
endif

set nrformats-=octal

" }}}
" Folding and comments {{{

if has('vim_starting')
  if has('folding')
    set foldmethod=marker
    set foldopen+=jump
  endif
  set commentstring=#\ %s
endif

" autocmd FileType c,cpp,cs,java        setlocal commentstring=//\ %s
" autocmd FileType desktop              setlocal commentstring=#\ %s
" autocmd FileType sql                  setlocal commentstring=--\ %s
" autocmd FileType xdefaults            setlocal commentstring=!%s
" autocmd FileType git,gitcommit        setlocal foldmethod=syntax foldlevel=1

" }}}
" Maps {{{

setglobal timeoutlen=1200
setglobal ttimeoutlen=50

if has('digraphs')
  digraph ,. 8230
  digraph cl 8984
endif

nnoremap Y  y$

inoremap <C-C> <Esc>`^

if exists(':xnoremap')
  xnoremap <Space> I<Space><Esc>gv
endif

nmap <script><silent><expr> <CR> &buftype ==# 'quickfix' ? "\r" : ":\025confirm " . (&buftype !=# 'terminal' ? (v:count ? 'write' : 'update') : &modified <Bar><Bar> exists('*jobwait') && jobwait([&channel], 0)[0] == -1 ? 'normal! i' : 'bdelete!') . "\r"

inoremap <M-o>      <C-O>o
inoremap <M-O>      <C-O>O
inoremap <M-i>      <Left>
inoremap <M-I>      <C-O>^
inoremap <M-A>      <C-O>$

nnoremap <silent> <C-w>z :wincmd z<Bar>cclose<Bar>lclose<CR>
nnoremap <silent> <C-w>Q :tabclose<CR>
nnoremap <silent> <C-w>, :if exists(':Wcd')<Bar>exe 'Wcd'<Bar>elseif exists(':Lcd')<Bar>exe 'Lcd'<Bar>elseif exists(':Glcd')<Bar>exe 'Glcd'<Bar>else<Bar>lcd %:h<Bar>endif<CR>
nmap cd <C-w>,

" Unknown
"
" if exists('&termwinkey')
"   tmap <script><expr> <SID>: (empty(&termwinkey) ? "\027" : eval('"\' . &termwinkey . '"')) . ':'
"   tmap <script><expr> <C-\>: (empty(&termwinkey) ? "\027" : eval('"\' . &termwinkey . '"')) . ':'
" elseif exists(':tmap')
"   tmap <script> <SID>: <C-\><C-N>:
"   tmap <script> <C-\>: <C-\><C-N>:
" endif

" Unknown
"
" function! s:MapEx(one, ...) abort
"   let rhs = join(a:000, ' ')
"   exe 'map  <script>' a:one '<C-\><C-N>:' . rhs . '<CR>'
"   exe 'cmap <script>' a:one '<C-\><C-N>:' . rhs . '<CR>'
"   exe 'imap <script>' a:one '<C-\><C-O>:' . rhs . '<CR>'
"   if exists(':tmap')
"     exe 'tmap <script>' a:one    '<SID>:' . rhs . '<CR>'
"   endif
"   return ''
" endfunction

vnoremap <S-Del> "+x
vnoremap <C-Insert> "+y
map  <script> <S-Insert> "+gP
map! <script> <S-Insert> <C-R><C-R>+
if has('eval')
  runtime! autoload/paste.vim
  if exists('g:paste#paste_cmd')
    exe 'imap <script> <S-Insert> <C-G>u' . g:paste#paste_cmd['i']
    exe 'vmap <script> <S-Insert> ' . g:paste#paste_cmd['v']
  endif
endif
if exists(':tmap')
  tmap <script><expr> <S-Insert> tr(@+, "\n", "\r")
  tmap <script><silent> <C-PageUp>   <SID>:tabprevious<CR>
  tmap <script><silent> <C-PageDown> <SID>:tabnext<CR>
endif

if !has('mac')
  map  <M-x> <S-Del>
  map! <M-x> <S-Del>
  map  <M-c> <C-Insert>
  map! <M-c> <C-Insert>
  map  <M-v> <S-Insert>
  map! <M-v> <S-Insert>
endif

" Unknown
"
" call s:MapEx('<C-F4>', 'confirm quit')
" call s:MapEx('<F28>', 'confirm quit')
" if !has('gui_running')
"   silent! execute "set <F28>=" . ($TERM =~# 'rxvt' ? "\e[14^" : "\e[1;5S")
" endif

" call s:MapEx('<C-S-PageUp>', '-tabmove')
" call s:MapEx('<C-S-PageDown>', '+tabmove')

" let s:mod = has('mac') ? 'D' : 'M'
" for s:i in range(1, 9)
"   exe 'noremap  <' . s:mod . '-' . s:i . '> <C-\><C-N>' . s:i . 'gt'
"   exe 'noremap! <' . s:mod . '-' . s:i . '> <C-\><C-N>' . s:i . 'gt'
"   if exists(':tmap')
"     exe 'tnoremap <' . s:mod . '-' . s:i . '> <C-w>:' . s:i . 'tabnext<CR>'
"   endif
" endfor

" }}}
" Reading and writing files {{{

setglobal autoread
setglobal autowrite
if has('multi_byte')
  let &g:fileencodings = substitute(&fileencodings, 'latin1', 'cp1252', '')
endif
setglobal fileformats=unix,dos,mac
setglobal backupskip+=/tmp/*

if exists('##CursorHold')
  autocmd CursorHold,BufWritePost,BufReadPost,BufLeave *
        \ if !$VIMSWAP && isdirectory(expand('<amatch>:h')) | let &swapfile = &modified | endif
endif

if has('vim_starting') && exists('+undofile')
  set undofile
endif

setglobal viminfo=!,'20,<50,s10,h
if !empty($SUDO_USER) && $USER !=# $SUDO_USER
  setglobal viminfo=
  setglobal directory-=~/tmp
  setglobal backupdir-=~/tmp
elseif exists('+undodir')
  if !empty($XDG_DATA_HOME)
    let s:data_home = substitute($XDG_DATA_HOME, '/$', '', '') . '/vim/'
  elseif has('win32')
    let s:data_home = expand('~/AppData/Local/vim/')
  else
    let s:data_home = expand('~/.local/share/vim/')
  endif
  let &undodir = s:data_home . 'undo//'
  let &directory = s:data_home . 'swap//'
  let &backupdir = s:data_home . 'backup//'
  if !isdirectory(&undodir) | call mkdir(&undodir, 'p') | endif
  if !isdirectory(&directory) | call mkdir(&directory, 'p') | endif
  if !isdirectory(&backupdir) | call mkdir(&backupdir, 'p') | endif
endif

" }}}
" Command line editing {{{

setglobal history=1000
setglobal wildmenu
" Default "full"
"
" setglobal wildmode=full
setglobal wildignore+=tags,.*.un~,*.pyc

cnoremap <C-O>      <Up>
cnoremap <C-R><C-L> <C-R>=substitute(getline('.'), '^\s*', '', '')<CR>

set cmdheight=2

" }}}
" External commands {{{

setglobal grepformat=%f:%l:%c:%m,%f:%l:%m,%f:%l%m,%f\ \ %l%m
if executable('rg')
  setglobal grepprg=rg\ -s\ --vimgrep
elseif has('unix')
  setglobal grepprg=grep\ -rn\ $*\ /dev/null
endif

" }}}
" Filetype settings {{{

" Unknown
"
" autocmd FileType * setlocal nolinebreak
" autocmd FileType sh,zsh,csh,tcsh,perl,python,ruby,tcl setlocal fo-=t |
"       \ if !&tw | setlocal tw=78 | endif
" autocmd FileType help setlocal ai formatoptions+=2n formatoptions-=ro
" autocmd FileType markdown,text setlocal linebreak keywordprg=dict
" autocmd FileType markdown if !&tw && expand('%:e') =~# '\<\%(md\|markdown\)\>' | setlocal tw=78 | endif
" autocmd FileType tex setlocal formatoptions+=l
" autocmd FileType vim setlocal keywordprg=:help |
"       \ if &foldmethod !=# 'diff' | setlocal foldmethod=expr foldlevel=1 | endif |
"       \ setlocal foldexpr=getline(v:lnum)=~'^\"\ Section:'?'>1':'='

" autocmd BufNewFile,BufRead *named.conf* setlocal ft=named
" autocmd BufWritePre,FileWritePre /etc/* if &ft == 'dns' |
"       \ exe "normal msHmt" |
"       \ exe "gl/^\\s*\\d\\+\\s*;\\s*Serial$/normal ^\<C-A>" |
"       \ exe "normal g`tztg`s" |
"       \ endif

" let g:sh_fold_enabled = has('folding')
" let g:is_posix = 1
" let g:go_fmt_autosave = 0
" let g:sql_type_default = 'pgsql'

" }}}
" Highlighting {{{

if !has('gui_running')
  " set Vim-specific sequences for RGB colors
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" Unknown
"
" if has('spell')
"   if has('vim_starting')
"     set spelllang=en_us
"     set spellfile=~/.vim/spell/en.utf-8.add
"     if &rtp =~# 'Dropbox.Code.vim'
"       set spellfile^=~/Dropbox/Code/vim/spell/en.utf-8.add
"     endif
"   endif
"   let g:spellfile_URL = 'http://ftp.vim.org/vim/runtime/spell'
"   autocmd FileType gitcommit setlocal spell
"   autocmd FileType help if &buftype ==# 'help' | setlocal nospell | endif
" endif

" if $TERM !~? 'linux' && &t_Co == 8
"   setglobal t_Co=16
" endif

if (&t_Co > 2 || has('gui_running')) && has('syntax')
  if !exists('syntax_on') && !exists('syntax_manual')
    exe 'augroup END'
    syntax on
    exe 'augroup my'
  endif
  if has('vim_starting')
    set list
    if !exists('g:colors_name')
      packadd! onedark.vim
      colorscheme onedark
    endif
  endif

  autocmd Syntax sh   syn sync minlines=500
  autocmd Syntax css  syn sync minlines=50
endif

" }}}
" Plugin settings {{{

imap <C-L>          <Plug>CapsLockToggle
imap <C-G>c         <Plug>CapsLockToggle

" Unknown
"
" let g:omni_sql_no_default_maps = 1
" let g:sh_noisk = 1
" let g:markdown_fenced_languages = ['ruby', 'html', 'javascript', 'css', 'bash=sh', 'sh']
" let g:liquid_highlight_types = g:markdown_fenced_languages + ['jinja=liquid', 'html+erb=eruby.html', 'html+jinja=liquid.html']

" Unknown
"
" let g:CSApprox_verbose_level = 0
" let g:NERDTreeHijackNetrw = 0
" let g:netrw_dirhistmax = 0
" let g:ragtag_global_maps = 1
" let b:surround_{char2nr('e')} = "\r\n}"
" let g:surround_{char2nr('-')} = "<% \r %>"
" let g:surround_{char2nr('=')} = "<%= \r %>"
" let g:surround_{char2nr('8')} = "/* \r */"
" let g:surround_{char2nr('s')} = " \r"
" let g:surround_{char2nr('^')} = "/^\r$/"
" let g:surround_indent = 1

function! LinterStatus() abort
  let l:counts = ale#statusline#Count(bufnr(''))

  let l:all_errors = l:counts.error + l:counts.style_error
  let l:all_non_errors = l:counts.total - l:all_errors

  return l:counts.total == 0 ? 'OK' : printf(
  \   '%dW %dE',
  \   all_non_errors,
  \   all_errors
  \)
endfunction

autocmd User Flags call Hoist("window", "LinterStatus")
autocmd User Flags call Hoist("global", "%{&ignorecase ? '[IC]' : ''}")

" }}}
" Misc {{{
"
setglobal sessionoptions-=buffers sessionoptions-=curdir sessionoptions+=sesdir,globals
" Unknown
"
" autocmd VimEnter * nested
"       \ if !argc() && empty(v:this_session) && filereadable('Session.vim') && !&modified |
"       \   source Session.vim |
"       \ endif

" }}}

" vim:set expandtab shiftwidth=2 foldmethod=marker:
